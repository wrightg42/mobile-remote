'use strict';
const PORT = process.env.PORT || 8080;
const BUILD_LOC = 'remote-site/build'

var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

// dictionary of host's and their IDs
var hostIDs = {};

// will get an ID for the host to use
function requestID() {
  var i = 0;
  while (true) {
    if (!(i in hostIDs)) {
      return i;
    }
  }
}

// chrome extension socket io connections
const host = io
  .of('/host')
  .on('connection', (socket) => {
    var id = undefined;

    socket.on('id', (fn) => {
      if (id == undefined) {
        // connected, generate host an ID
        id = requestID();
        hostIDs[id] = socket;
        fn(id);
        console.debug('host connected with id: %d', id);
      } else {
        // retransmit id if requested
        fn(id);
        console.debug('host with id %d forgot their id', id);
      }
    });

    socket.on('disconnect', () => {
      // host disconnected, remove from host IDs
      if (id != undefined) {
        console.debug('host disconnected with id: %d', id);
        delete hostIDs[id];
        id = undefined;
        socket.removeAllListeners();
      }
    });
  });

// mobile remote socket io connections
const remote = io
  .of('/remote')
  .on('connection', (socket) => {
    var hostSocket = null;

    socket.on('login', (id, pass, fn) => {
      console.debug('remote attempting login to host with id: %d', id);

      // check host exists
      if (id in hostIDs) {
        // send login to host
        var tmpHost = hostIDs[id];
        tmpHost.emit('login', pass, (response) => {
          // check response
          console.debug(response);
          if (response == 'ok') {
            hostSocket = tmpHost;

            // login was good, subscribe to events
            subscribeChannels(socket, hostSocket);
            hostSocket.on('password change', () => {
              console.debug('host changed password');
              socket.emit('host disconnect', 'Host has changed password');
              hostSocket = null;
            });

            hostSocket.on('disconnect', () => {
              console.debug('host connection lost');
              socket.emit('host disconnect', 'Lost connection to host');
              hostSocket = null;
            });
          }

          fn(response); // forward response back to remote
        });
      } else {
        console.debug('no host with this id, rejecting login');
        fn('Invalid host id'); // send error back to remote
      }
    });

    socket.on('disconnect', () => {
      console.debug('remote disconnected');
      socket.removeAllListeners();
      if (hostSocket !== null) {
        hostSocket.removeAllListeners();
      }
    });
  });

// subscribes to host communications allowing messages to be forwarded to the remotes and vica versa
function subscribeChannels(websocket, chromesocket) {
  // to send state from tab to the remote
  chromesocket.on('state', (state) => {
    console.debug('state: %O', state);
    websocket.emit('state', state);
  });

  // to active remote control tab
  websocket.on('power', (data) => {
    console.debug('power: %O', data);
    chromesocket.emit('power', data);
  });

  // to change if the video is playing or paused
  websocket.on('play', (data) => {
    console.debug('play: %O', data);
    chromesocket.emit('play', data);
  });

  // to change if the video is fullscreen or not
  websocket.on('fullscreen', (data) => {
    console.debug('fullscreen: %O', data);
    chromesocket.emit('fullscreen', data);
  });

  // to change time of the video, supports incremeant and seeking
  websocket.on('set time', (data) => {
    console.debug('set time: %O', data);
    chromesocket.emit('set time', data);
  });

  // to change volume of the video, supports incremeant and setting
  websocket.on('set volume', (data) => {
    console.debug('set volume: %O', data);
    chromesocket.emit('set volume', data);
  });

  // to change time of the video, supports incremeant and setting
  websocket.on('set speed', (data) => {
    console.debug('set speed: %O', data);
    chromesocket.emit('set speed', data);
  });
}

// host react web pages
app.use(express.static(path.join(__dirname, BUILD_LOC)));
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, BUILD_LOC, 'index.html'));
});

// start listening to connections
http.listen(PORT, () => {
  console.debug('server starting on port: %d', PORT);
});
