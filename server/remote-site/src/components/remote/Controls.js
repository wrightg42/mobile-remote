import React from 'react';
import { Button } from 'reactstrap';
import './Controls.css';

function SliderControl(props) {

}

function Control(props) {
  function clicked() {
    if (props.emitFunc !== undefined) {
      props.emitFunc(props.event, props.data);
    } else if (props.logoutFunc !== undefined) {
      props.logoutFunc();
    }
  }

  return(
    <Button
      id={ 'control-' + props.name }
      variant={ props.variant }
      onClick={ clicked }>
      { props.text }
    </Button>
  );
}

export {
  SliderControl,
  Control
};
