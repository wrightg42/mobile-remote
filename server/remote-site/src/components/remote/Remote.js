import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Control, SliderControl } from './Controls.js';
import './Remote.css';

function Remote(props) {
  return (
    <Container fluid>
      <Row>
        <Col>
          <Control
            name='logout'
            variant='primary'
            text='Logout'
            logoutFunc={ props.logoutFunc } />
        </Col>
        <Col>
          { props.data.active
            ? <Control
                name='off'
                variant='success'
                event='power'
                data={ { value: false } }
                text='Power off'
                emitFunc={ props.emitFunc } />
            : <Control
                name='on'
                variant='danger'
                event='power'
                data={ { value: true } }
                text='Power on'
                emitFunc={ props.emitFunc } />
            }
        </Col>
      </Row>
      <Row>
        <Col>
          { props.data.playing
           ? <Control
              name='pause'
              variant='success'
              event='play'
              data={ { value: false } }
              text='Pause'
              emitFunc={ props.emitFunc } />
            : <Control
               name='play'
               variant='success'
               event='play'
               data={ { value: true } }
               text='Play'
               emitFunc={ props.emitFunc } />
           }
        </Col>
        <Col>
          { props.data.fullscreen
           ? <Control
              name='exit-fullscreen'
              variant='success'
              event='fullscreen'
              data={ { value: false } }
              text='Exit Fullscreen'
              emitFunc={ props.emitFunc } />
            : <Control
               name='fullscreen'
               variant='success'
               event='fullscreen'
               data={ { value: true } }
               text='Fullscreen'
               emitFunc={ props.emitFunc } />
           }
        </Col>
      </Row>
    </Container>
  );
}

export default Remote;
