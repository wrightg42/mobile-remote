import io from 'socket.io-client';

// socket to connect to server
var socket = io('localhost:8080/remote', { autoConnect: false });
var chromeID = undefined;

// login function
function login(hostID, password, loginDataSetter, chromeDataSetter, hashPassword=true) {
  if (socket.connected) {
    disconnect();
  }

  // hash password if asked to
  if (hashPassword) {
    function hash(str) {
      // https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
      var hash = 0;
      for (var i = 0; i < str.length; ++i) {
        var chr = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0;
      }

      return hash.toString();
    }

    password = hash(password);
  }

  socket.open();
  socket.on('connect', () => {
    socket.emit('login', hostID, password, (response) => {
      console.debug(response);
      if (response === 'ok') {
        chromeID = hostID;
        loginDataSetter({
          id: hostID,
          password: '',
          loggedIn: true,
          error: { id: undefined, password: undefined }
        });

        subscribeSocketEvents(loginDataSetter, chromeDataSetter);
      } else {
        // set error to correct form item
        var error = { id: undefined, password: undefined }
        if (response === 'Invalid host id') {
          error.id = response;
        } else {
          error.password = response;
        }

        loginDataSetter({
          id: hostID,
          password: '',
          loggedIn: false,
          error: error
        });
      }
    });
  });
}

function logout(loginDataSetter) {
  disconnect();
  loginDataSetter({
    id: '',
    password: '',
    loggedIn: false,
    error: { id: undefined, password: undefined }
  });
}

// function to disconnect socket properly
function disconnect() {
  socket.removeAllListeners();
  socket.close();
}

// function to handle disconnect from host
function hostDisconnected(loginDataSetter) {
  socket.on('host disconnect', (cause) => {
    console.debug(cause);
    disconnect(); // host disconnected, disconnect from server to free resources

    // set error to correct form item
    var error = { id: undefined, password: undefined }
    if (cause === 'Lost connection to host') {
      error.id = cause;
    } else {
      error.password = cause;
    }

    loginDataSetter({
      id: chromeID,
      password: '',
      loggedIn: false,
      error: error
    });
  });

  socket.on('disconnect', () => {
    disconnect();

    var error = { id: 'Connection to server has been lost', password: undefined }
    loginDataSetter({
      id: chromeID,
      password: '',
      loggedIn: false,
      error: error
    });
  })
}

function statusUpdate(chromeDataSetter) {
  socket.on('state', (state) => {
    console.debug(state);
    chromeDataSetter(state);
  });
}

// subscribes to events
function subscribeSocketEvents(loginDataSetter, chromeDataSetter) {
  hostDisconnected(loginDataSetter);
  statusUpdate(chromeDataSetter);
}

function emitSignal(event, data) {
  console.debug('emitting %s: %O', event, data);
  socket.emit(event, data);
}

export {
  socket,
  login,
  logout,
  emitSignal
};
