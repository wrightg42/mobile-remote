import React from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button, FormFeedback } from 'reactstrap';
import './Login.css';

function Login(props) {
  return (
    <Row>
      <Col>
        <h2>Login</h2>
        <LoginForm data={ props.data } dataSetter={ props.dataSetter } loginFunc={ props.loginFunc } />
      </Col>
    </Row>
  );
}

function LoginForm(props) {
  function onDataChange(e) {
    // get what key has changed
    const key = e.target.name;
    const value = e.target.value;

    // clone data and set
    var clone = Object.assign({}, props.data);
    clone[key] = value;
    props.dataSetter(clone);
  }

  function attemptLogin(e) {
    // TODO: hash password
    var passwordHash = props.data.password;
    props.loginFunc(props.data.id, passwordHash);
    e.preventDefault();
  }

  return (
    <Form onSubmit={ attemptLogin }>
      <Field label='Host ID' name='id' type='text' pattern='[0-9]*' autoComplete='host-id' value={ props.data.id } onChange={ onDataChange } error={ props.data.error.id } />
      <Field label='Password' name='password' type='password' autoComplete='current-password' value={ props.data.password } onChange={ onDataChange } error={ props.data.error.password } />
      <FormGroup row>
        <Col sm={{ size: 10, offset: 2 }}>
          <Button name='connect' onClick={ attemptLogin }>
            Connect
          </Button>
        </Col>
      </FormGroup>
    </Form>
  );
}

function Field(props) {
  function handleChange(e) {
    if (e.target.validity.valid) {
      props.onChange(e);
    }
  }

  return (
    <FormGroup row>
      <Label for={ props.name } sm={2}>{ props.label }:</Label>
      <Col sm={10}>
        <Input
          id={ 'login-' + props.name }
          name={ props.name }
          type={ props.type }
          pattern={ props.pattern }
          autoComplete={ props.autoComplete }
          value={ props.value }
          onChange={ handleChange }
          invalid={ props.error !== undefined } />
        <FormFeedback>{ props.error }</FormFeedback>
      </Col>
    </FormGroup>
  );
}

function getURLParameters() {
  // get the varaibles in url
  var pageURL = window.location.search.substring(1);
  if (pageURL !== '') {
    var urlParameters = pageURL.split('&');

    var jsonString = '{';
    for (var i = 0; i < urlParameters.length; ++i) {
      // add parameter to dictionary
      var parameterParts = urlParameters[i].split('=');
      jsonString = jsonString.concat('"', parameterParts[0], '":"', parameterParts[1].replace('"', '\\"'), '",');
    }

    // replace last , with }
    jsonString = jsonString.replace(/.$/, '}');
    return JSON.parse(jsonString);
  } else {
    return JSON.parse('{}');
  }
}

// get params and login using credentials if provided
function getURLLoginDetails() {
  const params = getURLParameters();
  if ('id' in params) {
    // login with credentials
    if ('nopass' in params && params.nopass === 'true') {
      return {
        id: params.id,
        password: ''
      };
    } else if ('password' in params) {
      return {
        id: params.id,
        password: params.password
      };
    } else {
      // no password provided
      return {
        id: params.id,
        password: undefined
      };
    }
  }

  return {
    id: '',
    password: undefined
  };
}

export default Login;
export {
  getURLLoginDetails
};
