import React, { useState } from 'react';
import { Container } from 'reactstrap';

import Login, { getURLLoginDetails } from './components/login';
import Remote from './components/remote';
import * as socket from './components/socket/socket.js';

import './App.css';

function App() {
  // chrome extension state data
  const [chromeState, setChromeState] = useState(() => {
    return {
      active: false,
      hasVideo: false,
      playing: false,
      fullscreen: false,
      volume: 100,
      speed: 1.0,
      duration: 0,
      timestamp: 0
    }
  });

  // login state data
  var initpass = undefined;
  const [loginDetails, setLoginDetails] = useState(() => {
    // get login details from url
    var d = getURLLoginDetails();
    d.loggedIn = false;
    d.error = { id: undefined, password: undefined };

    // login if data provided, don't store password incase bad
    if (d.password !== undefined) {
      initpass = d.password;
    }

    d.password = '';

    return d;
  });

  // function to attempt login with server and subscribe to events
  function tryLogin(id, password, hashPassword=true) {
    socket.login(id, password, setLoginDetails, setChromeState, hashPassword);
  }

  // login first time if details provided in URL
  if (initpass !== undefined) {
    tryLogin(loginDetails.id, initpass, false);
  }

  // function to logout and set the details
  function logout() {
    socket.logout(setLoginDetails);
  }

  return (
    <div className="App">
      <div className="content">
        <Container fluid>
          { !loginDetails.loggedIn
            ? <Login data={ loginDetails } dataSetter={ setLoginDetails } loginFunc={ tryLogin } />
            : <Remote data={ chromeState } dataSetter={ setChromeState } emitFunc={ socket.emitSignal } logoutFunc={ logout } />
          }
        </Container>
      </div>
    </div>
  );
}

export default App;
