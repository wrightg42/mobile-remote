'use strict';
// socket data
var socket;
chrome.storage.local.get(['URL'], (result) => {
  const URL = result.URL;
  socket = io(URL + '/host', { autoConnect: false });
});

// sync the URL for the server when installing the chrome extension
chrome.runtime.onInstalled.addListener(() => {
  chrome.storage.local.set({ 'URL' : 'http://localhost:8080' }); // store url in global chrome storage
});

// internal state information
const startState = {
  connected: false,
  hostID: undefined,
  passwordHash: undefined,
  activeTab: false,
  tvWindowID: undefined,
  tvTabID: undefined
}

const startVideoState = {
  active: false,
  hasVideo: false,
  playing: false,
  fullscreen: false,
  volume: 1,
  speed: 1.0,
  duration: 0,
  timestamp: 0
}

var state = Object.assign({}, startState);
var videoState = Object.assign({}, startVideoState);

// sync with stored password
chrome.storage.local.get(['passwordHash'], (result) => {
  state.passwordHash = result.passwordHash;
});

// socket logic
const videoStateRequest = 'get video state';
function sendState() {
  sendTabMessage(videoStateRequest); // this gets state and will send to server
}

function subscribeChannels(port) {
  socket.on('connect', () => {
    clearTimeout(socket._connectTimer); // clear the timeout

    // request id once connected
    state.connected = true;
    socket.emit('id', (id) => {
      state.hostID = id;
      port.postMessage(state);
    });
  });

  socket.on('disconnect', lostConnection);

  socket.on('login', (loginPass, fn) => {
    console.debug('login: %O', loginPass);
    if (loginPass == state.passwordHash || state.passwordHash == null) {
      fn('ok');
      sendState();
    } else {
      fn('Invalid password');
    }
  });

  socket.on('power', (data) => {
    console.debug('power: %O', data);
    if (data.value === true) {
      openRemoteTab();
    } else {
      closeRemoteTab();
    }
  });

  socket.on('play', (data) => {
    console.debug('play: %O', data);
    sendTabMessage('play', data);
  });

  socket.on('fullscreen', (data) => {
    console.debug('fullscreen: %O', data);
    sendTabMessage('fullscreen', data);
  });

  socket.on('set time', (data) => {
    console.debug('set time: %O', data);
    sendTabMessage('seek', data);
  });

  socket.on('set volume', (data) => {
    console.debug('set volume: %O', data);
    sendTabMessage('set volume', data);
  });

  socket.on('set speed', (data) => {
    console.debug('set speed: %O', data);
    sendTabMessage('set speed', data);
  });
}

// shows error when connection to server is lost
function lostConnection() {
  // display disconnect error message if tab active
  if (state.activeTab) {
    var w = window.open('background/index.html', 'Connection Error');
    w.onload = () => {
      w.document.getElementById('heading').innerHTML = 'Connection Error';
      w.document.getElementById('text').innerHTML = 'Connection to server has been lost. Remotes will no longer work.' + state.activeTab ? '<br /><br /><br />Video will continue to play...' : '';
      w.focus();
      setTimeout(() => { w.close(); }, 5000);
    }
  }

  sendPopupError('Lost connection to server.');
  cleanupDisconnect(false);
  sendPopupState();
}

// function to cleanup state when disconnected
function cleanupDisconnect(closeTab=true) {
  socket.close();

  // close tv tab if active
  if (closeTab) {
    closeRemoteTab();
  }

  // reset state to represent disconnect
  state.connected = false;
  state.hostID = undefined;

  // stop listening to prevent issues with using old ports
  socket.removeAllListeners();
}

// closes remote tv tab
function closeRemoteTab() {
  if (state.activeTab) {
    chrome.tabs.remove(state.tvTabID); // no need to cleanup and send state, will be done by close event listener
  }
}

// opens a remote tv tab
function openRemoteTab() {
  if (!state.activeTab) {
    state.activeTab = true;

    // a function to listen for new window creation and to set it up properly
    function listener(newWindow) {
      // remove this listener so each new window doesn't get marked as the tv window
      chrome.windows.onCreated.removeListener(listener);

      // set state data
      state.tvWindowID = newWindow.id;
      videoState = Object.assign({}, startVideoState);
      videoState.active = true;
      sendState();
    }

    chrome.windows.onCreated.addListener(listener);
    chrome.windows.create({
      focused: true,
      state: 'maximized',
      url: 'background/index.html',
    });
  }
}

// handle the tv tab being closed
chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
  // if tab was closed, reset state information and emit power off
  if (tabId == state.tvTabID) {
    state.activeTab = false;
    state.tvWindowID = undefined;
    state.tvTabID = undefined;
    videoState = Object.assign({}, startVideoState);
    sendState();
  }
});

// a function to inject content script
var injected = false;
var cmdQueue = [];
function inject(tabId, changeInfo, tab) {
  if ((tabId === state.tvTabID || (tab.windowId === state.tvWindowID && state.tvTabID === undefined))) {
    if (changeInfo.status === 'complete') {
      injected = true;

      // set state and pin tab if first time tab has loaded
      if (state.tvTabID === undefined) {
        state.tvTabID = tabId;
        chrome.tabs.update(state.tvTabID, {
          pinned: true
        });
      } else {
        // inject the script otherwise, as not on intro page
        chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
          chrome.tabs.executeScript(state.tvTabID, {
            file: 'content/tab-manipulator.js'
          }, () => {
            // apply commands sent from queue
            cmdQueue.forEach((cmd) => { sendTabMessage(cmd.request, cmd.data, cmd.callback); });
            cmdQueue = [];

            // send state now js has been injected
            sendState();
          });
        });
      }
    } else if (changeInfo.status === 'loading') {
      injected = false;
      sendState();
    }
  }
}

// handle remote tab changing thus allowing for injections
chrome.tabs.onUpdated.addListener(inject);

// function to send a message to the tab requesting a change in the video
// this will always then send the state back to the server since this data will have changed
function sendTabMessage(request, data, callback=null) {
  if (videoState.active && injected) {
    chrome.tabs.query({ }, (tabs) => {
      chrome.tabs.sendMessage(state.tvTabID, { request: request, data: data }, (response) => {
        // get video state back and send this to server
        console.debug(response);
        videoState = Object.assign({}, response.state);
        socket.emit('state', videoState);
        if (callback !== null) {
          callback(response);
        }
      });
    });
  } else if (videoState.active) {
    // if was not a state request, add command to queue to apply when source is injected
    if (request !== videoStateRequest) {
      cmdQueue.push({ request: request, data: data, callback: callback });
    }

    // tab but not loaded the page, add request to queue and send empty data saying power is on
    var loadingState = Object.assign({}, startVideoState);
    loadingState.active = true;
    socket.emit('state', loadingState);
  } else {
    // no tab, send empty state
    socket.emit('state', startVideoState);
  }
}

// respond to changes from user in tab
chrome.runtime.onMessage.addListener((request, sender) => {
  if (sender.tab.id === state.tvTabID && request.changed === true) {
    // if from tab, and it is trying to get attention then request the state to send to server
    sendState();
  }
});

// communication with popup script
var ports = [];
function sendPopupState() {
  for (var i = 0; i < ports.length; ++i) {
    ports[i].postMessage(state);
  }
}

function sendPopupError(error) {
  for (var i = 0; i < ports.length; ++i) {
    ports[i].postMessage({ error: error });
  }
}

chrome.runtime.onConnect.addListener((port) => {
  // add to ports list
  var portNo = ports.length;
  ports.push(port);

  // port from list if popup closed
  port.onDisconnect.addListener(() => {
    ports.splice(portNo, 1);
  });

  port.onMessage.addListener((request) => {
    // check request and respond accordingly
    console.debug(request);
    if (request.type == 'connect') {
      // timeout function, so socket can tell if the server is down and thus cannot connect
      socket._connectTimer = setTimeout(function() {
        socket.close();
        cleanupDisconnect();
        sendPopupError('Failed to connect to server.');
      }, 2000);

      // connect socket and subscribe to channels
      socket.open();
      subscribeChannels(port);
    } else if (request.type == 'disconnect') {
      // close socket
      socket.removeAllListeners('disconnect'); // stop losing connection message as user forced close
      cleanupDisconnect();
      sendPopupState();
    } else if (request.type == 'set password') {
      // set local copy of password
      var password = request.data;
      if (password != '') {
        function hash(str) {
          // https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
          var hash = 0;
          for (var i = 0; i < str.length; ++i) {
            var chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
          }

          return hash.toString();
        }

        password = hash(password);
        state.passwordHash = password;
      } else {
        state.passwordHash = null;
      }

      // sync and send state back to popup
      chrome.storage.local.set({ 'passwordHash' : state.passwordHash }); // store for auto loading in future usage
      sendPopupState();

      // send message to server if password changed
      if (state.connected) {
        socket.emit('password change');
      }
    } else if (request.type == 'get state') {
      sendPopupState();
    }
  });
});
