'use strict';
$(document).ready(function() {
  // gets URL from storage
  var URL;
  chrome.storage.local.get(['URL'], (result) => {
    URL = result.URL;
  });

  // variables about current state of popup and extension
  var passSet = false;
  var windowId = undefined;
  var tabId = undefined;

  // functions to set popup content based off state information
  function handleState(state) {
    console.debug(state);

    // set password data
    if (!passSet) {
        if (state.passwordHash != null) {
          $('#password').attr('placeholder', '<unchanged>');
        }
    } else {
      $('#password').attr('placeholder', ''); // remove unchanged if user has removed password
    }

    // set host ID and QR code
    $('#host-id').text(state.hostID);

    // set which div is visable and state
    $('#gotoTab').addClass('hidden');
    $('#disconnect').addClass('full-width');
    if (state.connected) {
      $('#connect-container').addClass('hidden');
      $('#disconnect-container').removeClass('hidden');

      // create url to join this session
      var data = URL + '?id=' + state.hostID + '&nopass=true';
      if (state.passwordHash != undefined) {
        var data = URL + '?id=' + state.hostID + '&password=' + state.passwordHash.replace(' ', '%20');
      }

      // set qr code to join this session
      var typeNumber = 6;
      var errorCorrectionLevel = 'M';
      var qr = qrcode(typeNumber, errorCorrectionLevel);
      qr.addData(data);
      qr.make();
      $('#qrcode').html(qr.createImgTag());
      $('#qrcode').removeClass('hidden');
      $('#join-url').val(data);

      // set status and goto tab button
      if (state.activeTab) {
        $('#status').text('connected and active');
        $('#gotoTab').removeClass('hidden');
        $('#disconnect').removeClass('full-width');
      } else {
        $('#status').text('connected');
      }
    } else {
      $('#connect-container').removeClass('hidden');
      $('#disconnect-container').addClass('hidden');
      $('#qrcode').addClass('hidden');
      $('#status').text('not connected');
    }

    // set data for changing tab
    windowId = state.tvWindowID;
    tabId = state.tvTabID;
  }

  function handleError(response) {
    $('#ok-container').addClass('hidden');
    $('#error-container').removeClass('hidden');
    $('#error').text(response.error);
  }

  // port to communicate with background script
  var port = chrome.extension.connect({
    name: 'mobile-remote-host-background'
  });

  // function to send messages to background script
  function sendMessage(type, data, callback=null) {
    // send message
    port.postMessage({
      type: type,
      data: data
    });

    if (callback !== null) {
      port.onMessage.addListener(function handleResponse(response) {
        port.onMessage.removeListener(handleResponse); // prevent calling this function multiple times
        callback(response);
      });
    }
  }

  // handle messages from background script
  port.onMessage.addListener((data) => {
    // check no errors
    if (data.error) {
      handleError(data);
    } else {
      handleState(data); // set popup data from state data
    }
  });

  // handle popup.html interactions
  $('#connect').click(() => {
    sendMessage('connect', {});
  });

  $('#disconnect').click(() => {
    sendMessage('disconnect', {});
  });

  $('#close-error').click(() => {
    $('#error-container').addClass('hidden');
    $('#ok-container').removeClass('hidden');
  });

  $('#password').change(() => {
    passSet = true;
    sendMessage('set password', $('#password').val());
  });

  $('#copy-url').click(() => {
    navigator.clipboard.writeText($('#join-url').val());
  });

  $('#clearPassword').click(() => {
    passSet = true;
    $('#password').val('');
    sendMessage('set password', '');
  });

  $('#gotoTab').click(() => {
    // go to the correct window and tab
    chrome.windows.update(windowId, {
      focused: true,
      state: 'maximized'
    });

    chrome.tabs.update(tabId, {
      active: true
    });
  });

  // get initial state information from background script now popup has loaded
  sendMessage('get state', {});
});
