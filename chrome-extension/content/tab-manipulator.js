'use strict';
// get the default volume and speed from local storage
var volume;
var speed;
chrome.storage.local.get(['defaultVolume', 'defaultSpeed'], (result) => {
  volume = result.defaultVolume !== undefined ? result.defaultVolume : 1.0;
  speed = result.defaultSpeed !== undefined ? result.defaultSpeed : 1.0;
  if (video !== undefined) {
    setSpeed(video, speed);
    setVolume(video, volume);
  }
});

// gets the document/html inside an iframe
function getIframeDocument(iframeDOM) {
  return iframeDOM.contentDocument || iframeDOM.contentWindow.document;
}

// get the video from the page
function getPageVideos(page=document, checkIframes=true) {
  var videos = Array.from(page.getElementsByTagName('video'));
  videos = videos.map((v) => { return { video: v }; }); // mark all videos as not having an iframe

  // get videos from within iframe if checking
  if (checkIframes) {
    var iframes = Array.from(page.getElementsByTagName('iframe'));
    for (var i = 0; i < iframes.length; ++i) {
      try {
        var iframeDocument = getIframeDocument(iframes[i]);
        var iframeVideos = getPageVideos(iframeDocument);
        iframeVideos = iframeVideos.map((v) => { v.iframe = { video: iframes[i], iframe: v.iframe }; return v }); // attach iframe to video
        console.debug('iframe videos: %O', iframeVideos);
        videos = videos.concat(iframeVideos);
      } catch (error) {
        // unknown error getting videos from iframes, usually to do with cross site scripting
        console.debug(error);
      }
    }
  }

  // output for debug purpose
  if (page === document) {
    console.debug('videos: %O', videos);
  }

  return videos;
}

// sets a video based on videos detected in page
function setToVideo(i) {
  if (i >= 0 && i < videos.length) {
    // stop current playing if already selected
    var hadVideo = video !== undefined;
    if (hadVideo) {
      setPlay(video, false);
      unsubscribeVideoEvents(video);
    }

    video = videos[i];
    subscribeVideoEvents(video);

    // set to default volume and speed
    if (volume !== undefined) {
      setSpeed(video, speed);
      setVolume(video, volume);
    }

    // apply the command queue if it is a new video object
    if (!hadVideo) {
      applyCmdQueue(video);
    }
  } else {
    console.debug('ERROR: video selection out of bounds');
  }
}

// sets a video based on which is the longest detected on the page
function setToLongestVideo() {
  // find which video is longest
  var max = undefined;
  for (var i = 0; i < videos.length; ++i) {
    if (max === undefined || videos[i].video.duration > videos[max].video.duration) {
      max = i;
    }
  }

  // set playback video to longest on page
  if (max !== undefined) {
    setToVideo(max);
  } else {
    video = undefined;
  }
}

// get videos and the video
var videos = getPageVideos();
var video = undefined;
setToLongestVideo();

// gets data from current tab DOM to return info back to background script
function getData() {
  function emptyData() {
    return {
      active: true,
      hasVideo: false,
      playing: false,
      fullscreen: false,
      volume: volume,
      speed: speed,
      duration: 0,
      timestamp: 0
    };
  }

  // function to get data from a video
  function dataFromVideo(videoOBJ) {
    var state = emptyData(); // start with empty data

    // set state from video DOM
    state.hasVideo = true;
    state.playing = !videoOBJ.video.paused;
    state.volume = videoOBJ.video.volume;
    state.speed = videoOBJ.video.playbackRate;
    state.duration = videoOBJ.video.duration;
    state.timestamp = videoOBJ.video.currentTime;

    // check fullscreen element
    state.fullscreen = fullscreenVideoOBJ == videoOBJ;

    return state;
  }

  return video !== undefined ? dataFromVideo(video) : emptyData();
}

// functions to set video state
function setPlay(videoOBJ, playing) {
  // verify data passed to function
  if (videoOBJ === undefined) {
    return;
  }

  playing ? videoOBJ.video.play() : videoOBJ.video.pause();
}

var fullscreenVideoOBJ = undefined;
var scrollPos = undefined;
function setFullscreen(videoOBJ, fullscreen) {
  // verify data passed to function
  if (videoOBJ === undefined) {
    return;
  }

  // a function to add the css required for progromatic fullscreen to a document
  const fullscreenClassname = 'mobile-remote-fullscreen';
  function setCSSInDocument(enabled, page=document) {
    const id = 'zzzzzzzz-mobile-remote-css'; // add z's so less likly to conflict existing id's
    var stylesheet = page.getElementById(id)

    // add stylesheet to page if not found
    if (stylesheet === null) {
      stylesheet = document.createElement('style');
      stylesheet.setAttribute('id', id);
      stylesheet.type = 'text/css';
      stylesheet.innerHTML = '\
        .' + fullscreenClassname + ' {\
          clip: auto !important;\
          position: fixed !important;\
          clear: none !important;\
          top: 0 !important;\
          left: 0 !important;\
          min-width: 0 !important;\
          min-height: 0 !important;\
          width: 99.99% !important;\
          height: 99.99% !important;\
          max-width: 99.99% !important;\
          max-height: 99.99% !important;\
          margin: 0 !important;\
          padding: 0 !important;\
          visibility: visible !important;\
          border-width: 0 !important;\
          background: black !important;\
        }\
        \
        :not(.' + fullscreenClassname + ') {\
          visibility: hidden !important;\
          position: absolute !important;\
          left: 0 !important;\
        }\
        \
        body :not(.' + fullscreenClassname + ') {\
          overflow: hidden !important;\
        }';
      page.getElementsByTagName('head')[0].appendChild(stylesheet);
    }

    // enable or disable the stylesheet as required
    stylesheet.disabled = !enabled;
  }

  // make sure only 1 fullscreen element
  if (fullscreen && fullscreenVideoOBJ !== undefined) {
    setFullscreen(fullscreenVideoOBJ, false);
  }

  // add/remove classes and css files where required
  var prevScroll = { left: window.pageXOffset, top: window.pageYOffset };
  var currNode = videoOBJ;
  while (currNode !== undefined) {
    fullscreen ? currNode.video.classList.add(fullscreenClassname) : currNode.video.classList.remove(fullscreenClassname);
    setCSSInDocument(fullscreen, currNode.iframe === undefined ? document : getIframeDocument(currNode.iframe.video));
    currNode = currNode.iframe;
  }

  // set current fullscreen object
  if (fullscreen) {
    fullscreenVideoOBJ = videoOBJ;
    scrollPos = prevScroll;
  } else {
    fullscreenVideoOBJ = undefined;
    if (scrollPos !== undefined) {
      window.scrollTo(scrollPos.left, scrollPos.top);
      scrollPos = undefined;
    }
  }
}

function seek(videoOBJ, timestamp) {
  // verify data passed to function
  if (videoOBJ === undefined) {
    return;
  }

  videoOBJ.video.currentTime = timestamp;
}

// can be called with negative duration to skip backwards
function skipForward(videoOBJ, duration=10) {
  // verify data passed to function
  if (videoOBJ === undefined) {
    return;
  }

  videoOBJ.video.currentTime = videoDOM.currentTime + duration;
}

function setVolume(videoOBJ, value) {
  // verify data passed to function
  if (videoOBJ === undefined) {
    return;
  }

  // sync volume then set
  chrome.storage.local.set({ 'defaultVolume' : value });
  videoOBJ.video.volume = value;
}

function incVolume(videoOBJ, incremeant=10) {
  // verify data passed to function
  if (videoOBJ === undefined) {
    return;
  }

  videoOBJ.video.volume += incremeant;
  chrome.storage.local.set({ 'defaultVolume' : videoDOM.volume });
}

function setSpeed(videoOBJ, value) {
  // verify data passed to function
  if (videoOBJ === undefined) {
    return;
  }

  // sync speed then set
  chrome.storage.local.set({ 'defaultSpeed' : value });
  videoOBJ.video.playbackRate = value;
}

function incSpeed(videoOBJ, incremeant=10) {
  // verify data passed to function
  if (videoOBJ === undefined) {
    return;
  }

  videoOBJ.video.playbackRate += incremeant;
  chrome.storage.local.set({ 'defaultVolume' : videoDOM.playbackRate });
}

// function to handle requests to content script
var extensionID;
chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
  console.debug('request: %O', msg);
  if (extensionID === undefined) {
    extensionID = sender.id;
  }

  // only run command if from background script
  if (sender.id === extensionID) {
    if (video !== undefined) {
      handleRequest(video, msg);
    } else {
      addToCmdQueue(msg);
    }

    // always respond with state
    sendResponse({ state: getData() });
  }
});

function handleRequest(videoOBJ, msg) {
  unsubscribeVideoEvents(videoOBJ);
  if (msg.request === 'play') {
    setPlay(videoOBJ, msg.data.value);
  } else if (msg.request === 'fullscreen') {
    setFullscreen(videoOBJ, msg.data.value);
  } else if (msg.request === 'seek') {
    if (msg.data.seek === true) {
      seek(videoOBJ, msg.data.value);
    } else {
      skipForward(videoOBJ, msg.data.value);
    }
  } else if (msg.request === 'set volume') {
    if (msg.data.incremeant === true) {
      incVolume(videoOBJ, msg.data.value);
    } else {
      setVolume(videoOBJ, msg.data.value);
    }
  } else if (msg.request === 'set speed') {
    if (msg.data.incremeant === true) {
      incSpeed(videoOBJ, msg.data.value);
    } else {
      setSpeed(videoOBJ, msg.data.value);
    }
  }

  subscribeVideoEvents(videoOBJ);
}

var cmdQueue = [];
function addToCmdQueue(msg) {
  cmdQueue.push(msg);
}

function applyCmdQueue(videoOBJ) {
  if (cmdQueue !== undefined && cmdQueue !== []) {
    cmdQueue.forEach((cmd) => { handleRequest(videoOBJ, cmd); });
    sendToServer();
  }

  cmdQueue = [];
}

// changes must be sent back to server
function sendToServer() {
  if (extensionID !== undefined) {
    chrome.runtime.sendMessage(extensionID, { changed: true });

    // if fullscreen change was fired, our element can no longer be 'fullscreen'
    if (document.webkitFullscreenElement !== null) {
      if (fullscreenVideoOBJ !== undefined) {
        setFullscreen(fullscreenVideoOBJ, false);
      }
    }
  }
}

// detect user changes to the video
function subscribeVideoEvents(videoOBJ) {
  if (video !== undefined) {
    // play/pause
    videoOBJ.video.addEventListener('play', sendToServer);
    videoOBJ.video.addEventListener('pause', sendToServer);
    videoOBJ.video.addEventListener('playing', sendToServer);
    videoOBJ.video.addEventListener('waiting', sendToServer);

    // changed time, volume, speed
    videoOBJ.video.addEventListener('durationchange', sendToServer);
    videoOBJ.video.addEventListener('seeked', sendToServer);
    videoOBJ.video.addEventListener('volumechange', sendToServer);
    videoOBJ.video.addEventListener('ratechange', sendToServer);
  }
}

function unsubscribeVideoEvents(videoOBJ) {
  if (video !== undefined) {
    // play/pause
    videoOBJ.video.removeEventListener('play', sendToServer);
    videoOBJ.video.removeEventListener('pause', sendToServer);
    videoOBJ.video.removeEventListener('playing', sendToServer);
    videoOBJ.video.removeEventListener('waiting', sendToServer);

    // changed time, volume, speed
    videoOBJ.video.removeEventListener('durationchange', sendToServer);
    videoOBJ.video.removeEventListener('seeked', sendToServer);
    videoOBJ.video.removeEventListener('volumechange', sendToServer);
    videoOBJ.video.removeEventListener('ratechange', sendToServer);
  }
}

// observe mutations incase videos are added (netflix does this)
var observer = new MutationObserver((mutations) => {
  var insertedVideo = false;
  mutations.forEach((mutation) => {
    function newVideoInserted(node) {
      if (node.nodeName.toLowerCase() === 'video') {
        return true;
      }

      return Array.from(node.childNodes).some(n => newVideoInserted(n));
    }

    if (Array.from(mutation.addedNodes).some(newNode => newVideoInserted(newNode))) {
      insertedVideo = true;
    }
  });

  if (insertedVideo) {
    videos = getPageVideos();
    if (video === undefined) {
      setToLongestVideo();
    }
  }
});

observer.observe(document, { childList: true, subtree: true });
